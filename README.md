# sccca: Single-Cell Correlation Based Cell Type Annotation

[![CRAN RStudio mirror downloads](https://cranlogs.r-pkg.org/badges/grand-total/sccca?color=blue)](https://CRAN.R-project.org/package=sccca) 
[![CRAN RStudio mirror downloads](https://cranlogs.r-pkg.org/badges/sccca)](https://CRAN.R-project.org/package=sccca) 
[![](https://www.r-pkg.org/badges/version/sccca?color=green)](https://CRAN.R-project.org/package=sccca) 

# Installation

- Install from CRAN 
```R
install.packages("sccca")
```
- Install from GitLab
```R
library(devtools)
install_gitlab("mohamed.soudy/sccca")
```

# Description

Performing cell type annotation based on cell markers from a unified database. 
The approach utilizes correlation-based approach combined with association analayis using fisher-eaxct and phyper statistical tests.

![](https://i.ibb.co/zrNwH2j/Figure-1.jpg)

# Package information

- link to package on CRAN: [sccca](https://cran.r-project.org/package=sccca)

# Usage

**Example**

**Parameters**

- sobj: Seurat object.

- assay: assay to be used default is set to RNA.

- cluster: colname in the mata.data that have the cell cluster numbers.

- marker: cell markers database path.

- tissue: specified tissue from which the data comes.

- tt: tissue type whether 'a' for all types 'n' for normal tissues only or "c" for cancer tissues.

- cond: colname in the meta.data that have the condition names.

- m_t: overlap threshold between cell markers and expression matrix.

- c_t: correlation threshold between genes.

- test: statistical test that check if overlap is significant could be "p" for phyper or "f" for fisher.

- org: organism to be used that can be 'h' for human, 'm' for mouse, and 'a' for all markers.


```R
library(sccca)
library(SeuratData)

bmcite <- LoadData("bmcite")

bmcite@meta.data$clusters <- as.numeric(as.factor(bmcite@meta.data$celltype.l1))

sobj_markers <- sccca(bmcite, assay = "RNA", cluster = 'clusters',
                      cond = 'donor', markers = "Unified_markers_db.csv",
                      tissue = "Bone marrow", m_t = 0.9, c_t = 0.9,
                      test = "phyper", org = "h")
```
# Contribution Guidelines

For bugs and suggestions, the most effective way is by raising an issue on the gitlab issue tracker. Github allows you to classify your issues so that we know if it is a bug report, feature request or feedback to the authors.
